terraform {
  backend "gcs" {
    bucket = "koosio_tfstate"
    prefix = "terraform/state"
  }
}

provider "google" {
  version = "~> 1.16"
}

resource "google_project" "koosio" {
  name = "koosio"
  project_id = "koosio-210109"
  billing_account = "${data.google_billing_account.tchaypotech_billing.id}"
}

data "google_billing_account" "tchaypotech_billing" {
  billing_account = "00EF0B-BE1184-E69E91"
}
