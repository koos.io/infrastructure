# Pre-requisites

In order to use this repo, you'll need to install and configure:

 - [Terraform](https://www.terraform.io/downloads.html)
 - [Google Cloud SDK](https://cloud.google.com/sdk/)
 
You'll also need to have `devstorage.read_write scope` and `WRITER` permissions on the bucket used for storing terraform state.
 
# Initial setup

 - Use `gcloud auth application-default login` to set your default
   gCloud credentials.
 - Use `terraform init` inside the `terraform` folder to install the
   required plugins.
   
        $ terraform init

        Initializing provider plugins...
        - Checking for available provider plugins on https://releases.hashicorp.com...
        - Downloading plugin for provider "google" (1.16.0)...

        Terraform has been successfully initialized!

        You may now begin working with Terraform. Try running "terraform plan" to see any changes that are required for your infrastructure. All Terraform commands should now work.

$

